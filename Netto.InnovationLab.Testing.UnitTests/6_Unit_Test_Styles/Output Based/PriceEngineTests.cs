﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter;
using Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes;
using Netto.InnovationLab.Testing._6_Unit_Test_Styles.Output_Based;
using Xunit;
using static Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes.Rating;

namespace Netto.InnovationLab.Testing.UnitTests._6_Unit_Test_Styles.Output_Based
{
    [ExcludeFromCodeCoverage]
    public class PriceEngineTests
    {
        [Fact]
        [ProtectionAgainstRegression(Good)]
        [ResistanceToRefactoring(Good, Reason = "Only couples the system under test")]
        [FastFeedback(Good)]
        [Maintainablity(Good, Reason = "No state changes, so no out-of-process dependencies required")]
        public void Discount_of_two_products()
        {
            var product1 = new Product("Hand Wash");
            var product2 = new Product("Shampoo");
            var sut = new PriceEngine();
            
            var discount = sut.CalculateDiscount(product1, product2);
            
            discount.Should().Be(0.02m);
        }
    }
}