﻿using System.Diagnostics.CodeAnalysis;
using Netto.InnovationLab.Testing._5_Test_Doubles.Mock;
using Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes;
using NSubstitute;
using Xunit;
using static Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes.Rating;

namespace Netto.InnovationLab.Testing.UnitTests._6_Unit_Test_Styles.Communication_Based
{
    [ExcludeFromCodeCoverage]
    public class ControllerTests
    {
        [Fact]
        [ProtectionAgainstRegression(Moderate, Reason = "Overusing can result in shallow tests")]
        [ResistanceToRefactoring(Bad, Reason = @"Checkin interaction with stubs is always brittle. 
                                                 Mocks are fine only when the interaction cross application boundary 
                                                 and its side effects are visible externally")]
        [FastFeedback(Moderate, Reason = "Mocks can introduce additional latency")]
        [Maintainablity(Bad, Reason = @"Setting up test doubles and interaction assertion,
                                      which takes a lot of space. Possible mock chains.")]
        public void Sending_a_greetings_email()
        {
            const string testAddress = "user@email.com";
            var emailGatewayMock = Substitute.For<IEmailGateway>();
            var sut = new Controller(emailGatewayMock);
            
            sut.GreetUser(testAddress);
            
            emailGatewayMock.Received(1).SendGreetingsEmail(testAddress);
        }
    }
}