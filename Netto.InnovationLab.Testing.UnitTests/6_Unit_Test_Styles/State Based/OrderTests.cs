﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter;
using Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes;
using Netto.InnovationLab.Testing._6_Unit_Test_Styles.State_Based;
using Xunit;
using static Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes.Rating;

namespace Netto.InnovationLab.Testing.UnitTests._6_Unit_Test_Styles.State_Based
{
    [ExcludeFromCodeCoverage]
    public class OrderTests
    {
        [Fact]
        [ProtectionAgainstRegression(Good)]
        [ResistanceToRefactoring(Moderate, Reason = "Couples the system under test AND its state")]
        [FastFeedback(Good)]
        [Maintainablity(Moderate, Reason = "State verification can easily grow in size, thus harder to maintain")]
        public void Adding_a_product_to_an_order()
        {
            var product = new Product("Hand wash");
            var sut = new Order();
            
            sut.AddProduct(product);
            
            sut.Products.Should()
                .HaveCount(1)
                .And
                .ContainSingle(p => p.Equals(product));
        }

        [Fact]
        public void Adding_a_comment_as_reference_to_an_article()
        {
            var sut = new Article();
            const string text = "Comment text";
            const string author = "John Doe";
            var created = new DateTime(2019, 4, 1);
            
            sut.AddComment(text, author, created);

            sut.Comments.Should().HaveCount(1);
            sut.Comments[0].Text.Should().Be(text);
            sut.Comments[0].Author.Should().Be(author);
            sut.Comments[0].DateCreated.Should().Be(created);
        }

        [Fact]
        public void Adding_a_comment_as_value_object_to_an_article()
        {
            var sut = new Article();
            var comment = new Comment(
                "Comment Text",
                "John Doe",
                new DateTime(2019, 4, 1)
            );
            
            sut.AddComment(comment.Text, comment.Author, comment.DateCreated);
            
            sut.Comments.Should().BeEquivalentTo(comment);
        }
    }
}