﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test;
using NSubstitute;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._5_Test_Doubles._3_Mock_Als_Stub
{
    [ExcludeFromCodeCoverage]
    public class CustomerTests
    {
        [Fact]
        public void Purchase_fails_when_not_enough_inventory()
        {
            var storeMock = Substitute.For<IStore>();
            storeMock.HasEnoughInventory(Product.Shampoo, 5).Returns(false);
            var sut = new Customer();

            var success = sut.Purchase(storeMock, Product.Shampoo, 5);

            success.Should().BeFalse();
            storeMock.DidNotReceiveWithAnyArgs().RemoveInventory(default, default);
        }
    }
}