﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter;
using NSubstitute;
using Xunit;
using Customer = Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.Customer;
using IStore = Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.IStore;
using Product = Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.Product;

namespace Netto.InnovationLab.Testing.UnitTests._5_Test_Doubles._4_Intra_vs_Inter
{
    [ExcludeFromCodeCoverage]
    public class CustomerControllerTests
    {
        [Fact]
        public void Successful_purchase()
        {
            var emailGatewayMock = Substitute.For<IEmailGateway>();
            var sut = new CustomerController(emailGatewayMock);

            var success = sut.Purchase(1, 2, 5);

            success.Should().BeTrue();
            emailGatewayMock.Received(1).SendReceipt("customer@email.com", "Shampoo", 5);
        }

        [Fact]
        public void Purchase_succeeds_when_enough_inventory()
        {
            var storeMock = Substitute.For<IStore>();
            storeMock.HasEnoughInventory(Product.Shampoo, 5).Returns(true);
            var sut = new Customer();

            var success = sut.Purchase(storeMock, Product.Shampoo, 5);

            success.Should().BeTrue();
            storeMock.Received(1).RemoveInventory(Product.Shampoo, 5);
        }
    }
}