﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut;
using Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._1_Produkt_Kaufen;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._1_Invariant_Violation
{
    [ExcludeFromCodeCoverage]
    public class CustomerTest
    {
        [Fact]
        public void Purchase_succeeds_when_enough_inventory()
        { 
            // Arrange
            var store = new Store();
            store.AddInventory(Product.Shampoo, 10);
            var customer = new Customer(); 
            
            // Act
            var success = customer.Purchase(store, Product.Shampoo, 5);
            store.RemoveInventory(success, Product.Shampoo, 5); 
            
            // Assert
            success.Should().BeTrue();
            store.GetInventory(Product.Shampoo).Should().Be(5);
        }
    }
}