﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test;
using Xunit;
using static Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.Product;

namespace Netto.InnovationLab.Testing.UnitTests._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._3_Wiederverwendung_Von_Instanzen
{
    [ExcludeFromCodeCoverage]
    public class CustomerTests
    {
        [Fact]
        public void Purchase_WithMoreInventoryThanRequested_ShouldSucceed()
        {
            //TODO: Make object creation reusable
            var store = new Store();
            store.AddInventory(Shampoo, 10);
            var sut = new Customer();
            
            var success = sut.Purchase(store, Shampoo, 5);
            
            success.Should().BeTrue();
            store.GetInventory(Shampoo).Should().Be(5);
        }
        
        [Fact]
        public void Purchase_WithLessInventoryThanRequested_ShouldFail()
        {
            //TODO: Make object creation reusable
            var store = new Store();
            store.AddInventory(Shampoo, 10);
            var sut = new Customer();
            
            var success = sut.Purchase(store, Shampoo, 15);
            
            success.Should().BeFalse();
            store.GetInventory(Shampoo).Should().Be(10);
        }
    }
}