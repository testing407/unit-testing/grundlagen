﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test;
using NSubstitute;
using Xunit;
using static Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.Product;

namespace Netto.InnovationLab.Testing.UnitTests._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._2_Aufbau_und_Abriss
{
#nullable disable
    [ExcludeFromCodeCoverage]
    public class CustomerTests : IDisposable
    {
        private readonly Customer _sut;
        private readonly IStore _storeMock;

        public CustomerTests()
        {
            _sut = new Customer();
            _storeMock = Substitute.For<IStore>();
            _storeMock.HasEnoughInventory(Shampoo, 10).Returns(true);
        }

        [Fact]
        public void Purchase_WithMoreInventoryThanRequested_ShouldSucceed()
        {
            var success = _sut.Purchase(_storeMock, Shampoo, 5);

            success.Should().BeTrue();
            _storeMock.Received(1).RemoveInventory(Shampoo, 5);
        }


        [Fact]
        public void Purchase_WithLessInventoryThanRequested_ShouldFail()
        {
            _storeMock.HasEnoughInventory(Shampoo, 10).Returns(false);

            var success = _sut.Purchase(_storeMock, Shampoo, 15);

            success.Should().BeFalse();
            _storeMock.DidNotReceiveWithAnyArgs().RemoveInventory(Arg.Any<Product>(), Arg.Any<int>());
        }

        public void Dispose()
        {
            // _sut.CleanUp()
        }
    }
}