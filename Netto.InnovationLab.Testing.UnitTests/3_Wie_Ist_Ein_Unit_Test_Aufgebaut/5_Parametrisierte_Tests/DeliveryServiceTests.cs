﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._2_Lieferservice;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._5_Parametrisierte_Tests
{
    [ExcludeFromCodeCoverage]
    public class DeliveryServiceTests
    {
        [Fact]
        public void Delivery_with_a_past_date_is_invalid()
        {
            var sut = CreateDeliveryService();
            var delivery = CreateDeliveryWithDateDifference(-1);

            var isValid = sut.IsDeliveryValid(delivery);

            isValid.Should().BeFalse();
        }
        
        //TODO: Test yesterday, today, tomorrow and the day after tomorrow as delivery date

        private static Delivery CreateDeliveryWithDateDifference(int difference)
        {
            var date = DateTime.Now.AddDays(difference);
            return new Delivery(date);
        }


        private static DeliveryService CreateDeliveryService() => new();
    }
}