﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._2_Lieferservice;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._4_Testnamen
{
    [ExcludeFromCodeCoverage]
    public class DeliveryServiceTests
    {
        
        
        [Fact]
        //TODO: Give a better description of what is tested
        public void IsDeliveryValid_InvalidDate_ReturnFalse()
        {
            var sut = CreateDeliveryService();
            var delivery = CreateDeliveryWithDateDifference(-1);

            var isValid = sut.IsDeliveryValid(delivery);

            isValid.Should().BeFalse();
        }

        private static Delivery CreateDeliveryWithDateDifference(int difference)
        {
            var date = DateTime.Now.AddDays(difference);
            return new Delivery(date);
        }


        private static DeliveryService CreateDeliveryService() => new();
    }
}