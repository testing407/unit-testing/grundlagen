﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._2_Lieferservice;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._6_Generierte_Daten
{
    [ExcludeFromCodeCoverage]
    public class DeliveryServiceTests
    {
        [Theory]
        // TODO: directly use date instead of ints
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        public void Delivery_dates_after_tomorrow_are_valid(int daysFromNow)
        {
            var sut = CreateDeliveryService();
            var delivery = CreateDeliveryWithDateDifference(daysFromNow);

            var isValid = sut.IsDeliveryValid(delivery);

            isValid.Should().BeTrue();
        }
        
        [Theory]
        // TODO: directly use date instead of ints
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        public void Delivery_dates_before_the_day_after_tomorrow_are_invalid(int daysFromNow)
        {
            var sut = CreateDeliveryService();
            var delivery = CreateDeliveryWithDateDifference(daysFromNow);

            var isValid = sut.IsDeliveryValid(delivery);

            isValid.Should().BeFalse();
        }

        private static Delivery CreateDeliveryWithDateDifference(int difference)
        {
            var date = DateTime.Now.AddDays(difference);
            return new Delivery(date);
        }


        private static DeliveryService CreateDeliveryService() => new();
    }
}