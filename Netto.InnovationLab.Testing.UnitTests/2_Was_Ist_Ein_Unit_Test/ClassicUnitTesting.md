﻿```mermaid
sequenceDiagram
    participant T as Test
    participant C as Customer
    participant S as Store
    
    T ->>+ C: Purchase(store, product, amount): bool
    C ->>+ S: HasEnoughInventory(product, amount): bool
    S ->> S: _amountProduct >= amount
    S ->>- C: enoughInventory
    
    alt enoughInventory == false
    C ->> T: false
    else enoughInventory == true
    C ->>+ S: RemoveInventory(product, amount): void
    S ->> S: _amountProduct -= amount
    S ->>- C : _
    C ->>- T: true
    end
```