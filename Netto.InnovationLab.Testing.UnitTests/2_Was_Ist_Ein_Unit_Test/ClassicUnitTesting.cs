﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test;
using Xunit;
using static Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.Product;

namespace Netto.InnovationLab.Testing.UnitTests._2_Was_Ist_Ein_Unit_Test
{
    [ExcludeFromCodeCoverage]
    public class ClassicUnitTesting
    {
        [Fact]
        public void Purchase_WithMoreInventoryThanRequested_ShouldSucceed()
        {
            // Arrange
            var store = new Store();
            store.AddInventory(Shampoo, 10);
            var customer = new Customer();
            
            // Act
            var success = customer.Purchase(store, Shampoo, 5);
            
            // Assert
            success.Should().BeTrue();
            store.GetInventory(Shampoo).Should().Be(5);
        }

        [Fact]
        public void Purchase_WithLessInventoryThanRequested_ShouldFail()
        {
            // Arrange
            var store = new Store();
            store.AddInventory(Shampoo, 10);
            var customer = new Customer();

            // Act
            var success = customer.Purchase(store, Shampoo, 15);
            
            // Assert
            success.Should().BeFalse();
            store.GetInventory(Shampoo).Should().Be(10);
        }
    }
}