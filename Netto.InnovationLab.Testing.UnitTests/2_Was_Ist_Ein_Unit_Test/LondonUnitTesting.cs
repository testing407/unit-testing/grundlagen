﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test;
using NSubstitute;
using Xunit;
using static Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test.Product;

namespace Netto.InnovationLab.Testing.UnitTests._2_Was_Ist_Ein_Unit_Test
{
    [ExcludeFromCodeCoverage]
    public class LondonUnitTesting
    {
        [Fact]
        public void Purchase_WithMoreInventoryThanRequested_ShouldSucceed()
        {
            // Arrange
            var storeMock = Substitute.For<IStore>();
            storeMock.HasEnoughInventory(Shampoo, 5).Returns(true);
            var customer = new Customer();

            // Act
            var success = customer.Purchase(storeMock, Shampoo, 5);

            // Assert
            success.Should().BeTrue();
            storeMock.Received(1).RemoveInventory(Shampoo, 5);
        }

        [Fact]
        public void Purchase_WithLessInventoryThanRequested_ShouldSucceed()
        {
            // Arrange
            var storeMock = Substitute.For<IStore>();
            storeMock.HasEnoughInventory(Shampoo, 5).Returns(false);
            var customer = new Customer();

            // Act
            var success = customer.Purchase(storeMock, Shampoo, 5);

            // Assert
            success.Should().BeFalse();
            storeMock.DidNotReceiveWithAnyArgs().RemoveInventory(default, default);
        }
    }
}