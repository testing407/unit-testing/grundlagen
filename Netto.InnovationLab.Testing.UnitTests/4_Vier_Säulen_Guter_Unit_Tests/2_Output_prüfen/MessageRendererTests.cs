﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._4_Vier_Säulen_Guter_Unit_Tests._2_Output_prüfen
{
    [ExcludeFromCodeCoverage]
    public class MessageRendererTests
    {
        [Fact]
        public void Rendering_a_message()
        {
            var sut = new MessageRenderer();

            var renderers = sut.SubRenderers;
            
            //TODO: verify output 
            renderers.Should().HaveCount(3);
            renderers[0].Should().BeAssignableTo<HeaderRenderer>();
            renderers[1].Should().BeAssignableTo<BodyRenderer>();
            renderers[2].Should().BeAssignableTo<FooterRenderer>();
        }
    }
}