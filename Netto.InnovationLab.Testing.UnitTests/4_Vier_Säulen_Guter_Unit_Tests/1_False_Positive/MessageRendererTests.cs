﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests;
using Xunit;

namespace Netto.InnovationLab.Testing.UnitTests._4_Vier_Säulen_Guter_Unit_Tests._1_False_Positive
{
    [ExcludeFromCodeCoverage]
    public class MessageRendererTests
    {
        [Fact]
        public void MessageRenderer_uses_correct_sub_renderers()
        {
            var sut = new MessageRenderer();

            var renderers = sut.SubRenderers;

            renderers.Should().HaveCount(3);
            renderers[0].Should().BeAssignableTo<HeaderRenderer>();
            renderers[1].Should().BeAssignableTo<BodyRenderer>();
            renderers[2].Should().BeAssignableTo<FooterRenderer>();
        }
    }
}