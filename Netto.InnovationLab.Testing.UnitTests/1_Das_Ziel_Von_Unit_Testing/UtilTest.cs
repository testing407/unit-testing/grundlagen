﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;
using static Netto.InnovationLab.Testing._1_Das_Ziel_Von_Unit_Testing.Util;

namespace Netto.InnovationLab.Testing.UnitTests._1_Das_Ziel_Von_Unit_Testing
{
    [ExcludeFromCodeCoverage]
    public class UtilTest
    {
        [Fact]
        public void Test1()
        {
            var result = IsStringLong1("abc");
            
            result.Should().BeFalse();
        }
        
        [Fact]
        public void Test2()
        {
            var result = IsStringLong2("abc");
            
            result.Should().BeFalse();
        }
    }
}