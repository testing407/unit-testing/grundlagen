﻿namespace Netto.InnovationLab.Testing._1_Das_Ziel_Von_Unit_Testing
{
    public static class Util
    {
        private const int MinStringLongLength = 5;

        public static bool IsStringLong1(string input)
        {
            if (input.Length > MinStringLongLength)
                return true;
            return false;
        }
        public static bool IsStringLong2(string input)
        {
            return input.Length > MinStringLongLength;
        }
    }
}