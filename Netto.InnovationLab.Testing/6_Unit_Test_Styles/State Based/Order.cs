﻿using System.Collections.Generic;
using Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.State_Based
{
    public class Order
    {
        private readonly List<Product> _products = new();
        public IEnumerable<Product> Products => _products.AsReadOnly();

        public void AddProduct(Product product) => _products.Add(product);
    }
}