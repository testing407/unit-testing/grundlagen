﻿using System;
using System.Collections.Generic;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.State_Based
{
    public sealed class Article
    {
        private readonly List<Comment> _comments = new();

        public IList<Comment> Comments => _comments.AsReadOnly();

        public void AddComment(string text, string author, DateTime dateCreated)
        {
            var comment = new Comment(text, author, dateCreated);
            _comments.Add(comment);
        }
    }
}