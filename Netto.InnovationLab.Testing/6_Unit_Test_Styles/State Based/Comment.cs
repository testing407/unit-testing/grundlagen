﻿using System;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.State_Based
{
    public record Comment(string Text, string Author, DateTime DateCreated);
}