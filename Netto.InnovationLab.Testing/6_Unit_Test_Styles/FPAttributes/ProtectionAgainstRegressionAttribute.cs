﻿using System.Diagnostics.CodeAnalysis;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes
{
    [ExcludeFromCodeCoverage]
    public sealed class ProtectionAgainstRegressionAttribute : RatingAttribute
    {
        public ProtectionAgainstRegressionAttribute(Rating rating) : base(rating)
        {
        }
    }
}