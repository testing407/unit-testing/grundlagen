﻿using System.Diagnostics.CodeAnalysis;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes
{
    [ExcludeFromCodeCoverage]
    public sealed class FastFeedbackAttribute : RatingAttribute
    {
        public FastFeedbackAttribute(Rating rating) : base(rating)
        {
        }
    }
}