﻿using System.Diagnostics.CodeAnalysis;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes
{
    [ExcludeFromCodeCoverage]
    public class MaintainablityAttribute : RatingAttribute
    {
        public MaintainablityAttribute(Rating rating) : base(rating)
        {
        }
    }
}