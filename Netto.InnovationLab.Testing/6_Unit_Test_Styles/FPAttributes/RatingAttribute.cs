﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes
{
    [ExcludeFromCodeCoverage]
    public abstract class RatingAttribute : Attribute
    {
        private Rating _rating;
        public string? Reason;

        protected RatingAttribute(
            Rating rating,
            [Optional] [DefaultParameterValue(null)]
            string? reason
        ) => (_rating, Reason) = (rating, reason);
    }
}