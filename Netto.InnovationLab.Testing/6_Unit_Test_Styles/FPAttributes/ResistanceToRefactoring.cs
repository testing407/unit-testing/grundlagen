﻿using System.Diagnostics.CodeAnalysis;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes
{
    [ExcludeFromCodeCoverage]
    public sealed class ResistanceToRefactoring : RatingAttribute
    {
        public ResistanceToRefactoring(Rating rating) : base(rating)
        {
        }
    }
}