﻿namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.FPAttributes
{
    public enum Rating
    {
        Bad,
        Moderate,
        Good
    }
}