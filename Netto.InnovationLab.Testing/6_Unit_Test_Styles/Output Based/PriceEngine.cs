﻿using System;
using Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter;

namespace Netto.InnovationLab.Testing._6_Unit_Test_Styles.Output_Based
{
    public class PriceEngine
    {
        public decimal CalculateDiscount(params Product[] products)
        {
            var discount = products.Length * 0.01m;
            return Math.Min(discount, 0.2m);
        }
    }
}