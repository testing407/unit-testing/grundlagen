﻿using System;

namespace Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._2_Lieferservice
{
    public class DeliveryService
    {
        public bool IsDeliveryValid(Delivery delivery) => delivery.Date >= DateTime.Now.AddDays(2).Date;
    }
}