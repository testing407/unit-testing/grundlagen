﻿using System;

namespace Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._2_Lieferservice
{
    public record Delivery(DateTime Date);
}