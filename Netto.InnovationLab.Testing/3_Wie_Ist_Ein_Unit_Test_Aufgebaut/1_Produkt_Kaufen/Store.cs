﻿using System;

namespace Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._1_Produkt_Kaufen
{
    public class Store : IStore
    {
        private int _amountShampoo;
        private int _amountBooks;
        
        public void AddInventory(Product product, int amount)
        {
            if (amount <= 0)
            {
                return;
            }
            
            switch (product)
            {
                case Product.Shampoo: 
                    _amountShampoo += amount;
                    break;
                case Product.Book:
                    _amountBooks += amount;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(product), product, null);
            }
        }

        public int GetInventory(Product product)
        {
            return product switch
            {
                Product.Shampoo => _amountShampoo,
                Product.Book => _amountBooks,
                _ => throw new ArgumentOutOfRangeException(nameof(product), product, null)
            };
        }

        public bool HasEnoughInventory(Product product, int amount)
        {
            return product switch
            {
                Product.Shampoo => HasEnoughInventoryInternal(_amountShampoo, amount),
                Product.Book => HasEnoughInventoryInternal(_amountBooks, amount),
                _ => throw new ArgumentOutOfRangeException(nameof(product), product, null)
            };
        }

        private static bool HasEnoughInventoryInternal(int product, int amount)
        {
            return product >= amount;
        }

        public void RemoveInventory(bool success, Product product, int amount)
        {
            if (!success)
            {
                return;
            }
            
            switch (product)
            {
                case Product.Shampoo:
                    _amountShampoo -= amount;
                    break;
                case Product.Book:
                    _amountBooks -= amount;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(product), product, null);
            }
        }
    }
}