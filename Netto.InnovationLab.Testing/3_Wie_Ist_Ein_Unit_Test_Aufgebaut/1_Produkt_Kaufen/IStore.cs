﻿namespace Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._1_Produkt_Kaufen
{
    public interface IStore
    {
        void AddInventory(Product product, int amount);
        int GetInventory(Product product);
        bool HasEnoughInventory(Product product, int amount);
        void RemoveInventory(bool success, Product product, int amount);
    }
}