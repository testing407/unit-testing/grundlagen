﻿namespace Netto.InnovationLab.Testing._3_Wie_Ist_Ein_Unit_Test_Aufgebaut._1_Produkt_Kaufen
{
    public class Customer
    {
        public bool Purchase(IStore store, Product product, int amount)
        {
            return store.HasEnoughInventory(product, amount);
        }
    }
}