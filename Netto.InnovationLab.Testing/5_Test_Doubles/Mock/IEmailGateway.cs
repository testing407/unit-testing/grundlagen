﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Mock
{
    public interface IEmailGateway
    {
        public void SendGreetingsEmail(string address);
    }
}