﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Mock
{
    public class Controller
    {
        private readonly IEmailGateway _emailGateway;

        public Controller(IEmailGateway emailGateway) => _emailGateway = emailGateway;

        public void GreetUser(string address) => _emailGateway.SendGreetingsEmail(address);
    }
}