﻿using System;

namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public class CustomerRepository : ICustomerRepository
    {
        public Customer GetById(int customerId) => customerId == 1 ? new Customer("customer@email.com") : throw new ArgumentException(nameof(CustomerRepository));
    }
}