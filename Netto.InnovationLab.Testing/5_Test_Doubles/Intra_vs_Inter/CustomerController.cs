﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public class CustomerController
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IProductRepository _productRepository;
        private readonly IEmailGateway _emailGateway;
        private readonly IStore _mainStore;

        public CustomerController(IEmailGateway emailGateway)
        {
            _customerRepository = new CustomerRepository();
            _productRepository = new ProductRepository();
            _mainStore = new MainStore();
            _emailGateway = emailGateway;
        }

        public bool Purchase(int customerId, int productId, int quantity)
        {
            var customer = _customerRepository.GetById(customerId);
            var product = _productRepository.GetById(productId);

            var isSuccess = customer.Purchase(_mainStore, product, quantity);

            if (isSuccess)
            {
                _emailGateway.SendReceipt(customer.Email, product.Name, quantity);
            }

            return isSuccess;
        }
    }
}