﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public class Customer
    {
        public Customer(string email)
        {
            Email = email;
        }

        public string Email { get; }

        public bool Purchase(IStore store, Product product, int quantity)
        {
            if (!store.HasEnoughInventory(product, quantity)) 
                return false;
            
            store.RemoveInventory(product, quantity);
            return true;

        }
    }
}