﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public record Product(string Name);
}