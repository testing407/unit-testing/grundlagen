﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public interface IEmailGateway
    {
        void SendReceipt(string email, string product, int quantity);
    }
}