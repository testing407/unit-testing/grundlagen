﻿using System;

namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public class ProductRepository : IProductRepository
    {
        public Product GetById(int productId) => productId == 2 ? new Product("Shampoo") : throw new ArgumentException(nameof(ProductRepository));
    }
}