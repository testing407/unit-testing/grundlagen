﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Intra_vs_Inter
{
    public class MainStore : IStore
    {
        public bool HasEnoughInventory(Product product, int quantity) => quantity <= 10;

        public void RemoveInventory(Product product, int quantity)
        {
        }
    }
}