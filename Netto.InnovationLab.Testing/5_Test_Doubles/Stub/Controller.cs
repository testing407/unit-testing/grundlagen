﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Stub
{
    public class Controller
    {
        private readonly IDataBase _dataBase;

        public Controller(IDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public Report CreateReport()
        {
            var numberOfUsers = _dataBase.GetNumberOfUsers();
            return new Report(numberOfUsers);
        }
    }
}