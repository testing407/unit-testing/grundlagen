﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Stub
{
    public interface IDataBase
    {
        int GetNumberOfUsers();
    }
}