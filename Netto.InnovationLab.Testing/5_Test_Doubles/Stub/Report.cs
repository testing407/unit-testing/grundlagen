﻿namespace Netto.InnovationLab.Testing._5_Test_Doubles.Stub
{
    public record Report(int NumberOfUsers);
}