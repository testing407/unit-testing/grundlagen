﻿namespace Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test
{
    public class Customer
    {
        public bool Purchase(IStore store, Product product, int amount)
        {
            if (!store.HasEnoughInventory(product, amount))
                return false;
                
            store.RemoveInventory(product, amount);
            return true;
        }
    }
}