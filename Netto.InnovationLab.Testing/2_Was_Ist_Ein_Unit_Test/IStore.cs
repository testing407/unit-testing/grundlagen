﻿namespace Netto.InnovationLab.Testing._2_Was_Ist_Ein_Unit_Test
{
    public interface IStore
    {
        void AddInventory(Product product, int amount);
        int GetInventory(Product product);
        bool HasEnoughInventory(Product product, int amount);
        void RemoveInventory(Product product, int amount);
    }
}