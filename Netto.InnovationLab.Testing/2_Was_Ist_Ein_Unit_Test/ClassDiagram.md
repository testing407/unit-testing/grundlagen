```mermaid
classDiagram
    class Customer {
        +Purchase(IStore store, Product product, int amount) bool
    }

    class IStore{
    <<interface>>
        +AddInventory(Product product, int amount) void
        +GetInventory(Product product) int
        +HasEnoughInventory(Product product, int amount) bool
        +RemoveInventory(Product product, int amount) void
    }
    
    class Product{
        <<enumeration>>
        Shampoo
        Book
    }

    class Store{
        -_amountShampoo : int
        -_amountBooks : int
        -HasEnoughInventoryInternal(int product, int amount)$ bool
    }

    Customer "1" --> "1" Product: Buys
    IStore "1" --> "1" Product : Sells

    IStore <.. Customer : Interact
    IStore <|.. Store : Implements
```