﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Netto.InnovationLab.Testing
{
    [ExcludeFromCodeCoverage]
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}