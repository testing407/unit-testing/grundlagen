﻿namespace Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests
{
    public interface IRenderer
    {
        string Render(Message message);
    }
}