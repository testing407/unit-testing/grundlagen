﻿using System.Collections.Generic;
using System.Linq;

namespace Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests
{
    public class MessageRenderer : IRenderer
    {
        public MessageRenderer()
        {
            SubRenderers = new List<IRenderer>
            {
                new HeaderRenderer(),
                new BodyRenderer(), // Note: Replace with BoldRenderer 
                new FooterRenderer()
            };
        }

        public IReadOnlyList<IRenderer> SubRenderers { get; }

        public string Render(Message message) =>
            SubRenderers
                .Select(x => x.Render(message))
                .Aggregate("", (str1, str2) => str1 + str2);
    }
}