﻿namespace Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests
{
    public record Message(string Header, string Body, string Footer);
}