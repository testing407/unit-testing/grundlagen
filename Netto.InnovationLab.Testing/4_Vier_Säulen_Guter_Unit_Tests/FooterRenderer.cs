﻿namespace Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests
{
    public class FooterRenderer : IRenderer
    {
        public string Render(Message message) => $"<h1>{message.Footer}</h1>"; //Note: $"<i>{message.Footer}</i>"
    }
}