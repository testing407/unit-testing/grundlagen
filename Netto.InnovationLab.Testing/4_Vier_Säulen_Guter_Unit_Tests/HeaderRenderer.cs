﻿namespace Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests
{
    public class HeaderRenderer : IRenderer
    {
        public string Render(Message message) => $"<i>{message.Header}</i>"; //Note: $"<h1>{message.Header}</h1>"
    }
}