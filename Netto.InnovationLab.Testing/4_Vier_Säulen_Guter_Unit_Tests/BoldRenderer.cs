﻿namespace Netto.InnovationLab.Testing._4_Vier_Säulen_Guter_Unit_Tests
{
    public class BoldRenderer : IRenderer
    {
        public string Render(Message message) => $"<b>{message.Body}</b>";
    }
}